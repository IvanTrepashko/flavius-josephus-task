﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace FlaviusJosephusTask
{
    /// <summary>
    /// Represents class for solving Flavius Josephus problem.
    /// </summary>
    public static class FlaviusJosephusProblem
    {
        /// <summary>
        /// Gets the safe position.
        /// </summary>
        /// <param name="numberOfPeople">Number of people.</param>
        /// <param name="rotation">Rotation.</param>
        /// <returns>Safe position.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when <param name="numberOfPeople"></param>
        /// or <param name="rotation"></param> is less or equal to 0.</exception>
        public static int GetSafePosition(int numberOfPeople, int rotation)
        {
            CheckParameters(numberOfPeople, rotation);

            BitArray people = new BitArray(numberOfPeople, false);
            int remaining = numberOfPeople;
            int index = 0;
            
            while (true)
            {
                for (int i = 0; i < numberOfPeople; i++)
                {
                    if (people[i])
                    {
                        continue;
                    }
                    
                    index++;

                    if (index % rotation == 0)
                    {
                        people[i] = true;
                        remaining--;
                    }

                    if (remaining == 1)
                    {
                        return people.IndexOf(false) + 1;
                    }
                }
            }
        }
        
        /// <summary>
        /// Returns sequence of unsafe positions.
        /// </summary>
        /// <param name="numberOfPeople">Number of people.</param>
        /// <param name="rotation">Rotation.</param>
        /// <returns>Sequence of unsafe positions.</returns>
        public static IEnumerable<int> GetPositions(int numberOfPeople, int rotation)
        {
            CheckParameters(numberOfPeople, rotation);
            
            BitArray people = new BitArray(numberOfPeople);
            int remaining = numberOfPeople;
            int index = 0;
            
            while(remaining != 1)
            {
                for (int i = 0; i < numberOfPeople; i++)
                {
                    if (people[i])
                    {
                        continue;
                    }
                    
                    index++;

                    if (index % rotation == 0)
                    {
                        people[i] = true;
                        remaining--;
                        yield return i + 1;
                    }
                }
            }
        }

        private static void CheckParameters(int numberOfPeople, int rotation)
        {
            if (numberOfPeople <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(numberOfPeople));
            }

            if (rotation <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(rotation));
            }
        }

        private static int IndexOf(this BitArray array, bool value)
        {
            for (int i = 0; i < array.Count; i++)
            {
                if (array[i] == value)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}