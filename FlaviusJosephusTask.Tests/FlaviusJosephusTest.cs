using System;
using NUnit.Framework;

namespace FlaviusJosephusTask.Tests
{
    public class FlaviusJosephusTest
    {
        [TestCase(10, 3, 4)]
        [TestCase(14, 2, 13)]
        [TestCase(13, 2, 11)]
        [TestCase(1, 5, 1)]
        [TestCase(15, 2, 15)]
        [TestCase(13, 1, 13)]
        [TestCase(40, 7, 24)]
        [TestCase(1000, 123, 2)]
        [TestCase(1000, 1000, 609)]
        [TestCase(66, 100, 7)]
        public void GetSafePosition_ReturnsPosition(int numberOfPeople, int rotation, int expected )
        {
            int actual = FlaviusJosephusProblem.GetSafePosition(numberOfPeople, rotation);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetSafePosition_InvalidNumberOfPeople_ThrowsArgumentOutOfRangeException()
            => Assert.Throws<ArgumentOutOfRangeException>(() => FlaviusJosephusProblem.GetSafePosition(-10, 4));
        
        [Test]
        public void GetSafePosition_InvalidRotation_ThrowsArgumentOutOfRangeException()
            => Assert.Throws<ArgumentOutOfRangeException>(() => FlaviusJosephusProblem.GetSafePosition(10, 0));

        [Test]
        public void GetPositions_ReturnsUnsafePositions()
        {
            int[] expected = {3, 6, 9, 2, 7, 1, 8, 5, 10};

            var actual = FlaviusJosephusProblem.GetPositions(10, 3);
            
            Assert.AreEqual(expected, actual);
        }
    }
}